import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'oorsi-web-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.css']
})
export class TermsAndConditionsComponent implements OnInit {

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  termsAndConditions: any = '';

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    debugger;
    this.http.get(environment.apiEndpoint + 'termsAndConditions', { responseType: 'text' }).subscribe(data => { this.termsAndConditions = data }, error => {
      this.http.get('assets/html/termsAndConditions.html', { responseType: 'text' }).subscribe(data => this.termsAndConditions = data)
    });
  }

  onCancel() {
    this.cancel.emit();
  }

}
