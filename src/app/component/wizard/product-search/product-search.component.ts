import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product';
import { ProductService } from 'src/app/service/product/product.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { WishlistService } from "../../../service/wishlist.service";
import { AuthService } from "../../../service/auth/auth.service";

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.css']
})
export class ProductSearchComponent implements OnInit {

  public innerWidth: number;

  itemsPerSlide: number;

  products: Product[] = [];
  searchString: string = '';
  isAddedToWishList = false;
  
  constructor(private productService: ProductService, private http: HttpClient,
    private wishlistService: WishlistService,
    private authService: AuthService,) {
  }

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    this.itemsPerSlide = Math.floor(this.innerWidth / 300);
    if (environment.production != true)
      this.http.get<Product[]>('assets/test/products.json').subscribe(products => this.products = products);
  }

  onSearch(): void {
    this.products = undefined;
    this.productService.search(this.searchString).subscribe(data => {
      /** pass recieved data to propery */
      this.products = data;
    });
  }

  addToWishlist(i){
    this.wishlistService.addProductToWishlist(this.products[i]).subscribe(
      /** success - it is added to wishlist */
      data => {this.isAddedToWishList = true},
      /** error - to be checked by auth service */
      err => this.authService.checkError(err)
    );
   
    
  }


}
