import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.css']
})
export class WizardComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    var routers = this.router.url;

    if (routers == "/welcome/addPhone") {
      document.getElementById("second").className = "active";
    }
    else if (routers == '/welcome/addBirthday') {
      document.getElementById("second").className = "active";
      document.getElementById("third").className = "active";
    }

  }
}
